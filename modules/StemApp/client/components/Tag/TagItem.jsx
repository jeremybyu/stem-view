import { Component, PropTypes } from 'react'

export default class TagItem extends Component {
  static propTypes = {
    tag: PropTypes.object.isRequired,
    user: PropTypes.object,
    auth: PropTypes.bool.isRequired
  };

  handleChecked (e) {
    // Prevent default otherwise click would fire twice due to fast-click & react acting on it (f7 incl. fast-click)
    e.preventDefault()
    // Can not use e.target.checked because iOS does not fire onChange events
    // on input fields inside of labels when fast-click is in use. So we use a ref.
    // Set the checked property to the opposite of its current value
    // https://facebook.github.io/react/docs/forms.html#potential-issues-with-checkboxes-and-radio-buttons
    //  Meteor.call('setChecked', this.props.task._id, !this.refs['checkbox'].checked)
  }

  handleDelete (e) {
    // Prevent default otherwise click would fire twice due to fast-click & react
    e.preventDefault()
    Meteor.call('DeleteTag', this.props.tag._id)
    //  Meteor.call('deleteTag', this.props.tag._id)
    //  console.log('Need to handle Delete')
  }
  //  Make this into an edit view
  handleEdit (e) {
    // Prevent default otherwise click would fire twice due to fast-click & react
    e.preventDefault()
    //  //  console.log(`Need to change to edit view and pass tag_id ${this.props.tag._id}`)
    // TODO figure out how to get rid of the use of Session Variables and Global Variables
    Session.set('form_id', this.props.tag._id)
  }

  render () {
    let swipeContent = (<div className='swipeout-actions-right'><a href='#' className='bg-yellow'>Please Sign In</a></div>)
    if (this.props.auth) {
      swipeContent = (
        <div className='swipeout-actions-right'>
          <a href='#form' className='bg-blue' style={{}} onClick={this.handleEdit.bind(this)}>
            Edit
          </a>
          <a href='#' className='bg-red'
             style={{}}
             onClick={this.handleDelete.bind(this)}>Delete
           </a>
        </div>
      )
    }
    //  Need to figure out how to dynamically determine how much to truncate.
    let charsToDisplay = 15
    let shortDescription = this.props.tag.description.substring(0, charsToDisplay) + '...'
    return (
      <li className='swipeout' >
        <div className='swipeout-content item-content' style={{}}>
          <div className='item-inner'>
            <div className='item-title'>
              Tag: {this.props.tag.tag}
            </div>
            <div className='item-after'>
              {shortDescription}
              <span className='badge'>{this.props.tag.params.length}</span>
            </div>
          </div>
        </div>
        {swipeContent}
      </li>
    )
  }
}
