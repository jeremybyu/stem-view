import Formsy from 'formsy-react'
import { Component, PropTypes } from 'react'
import ReactMixin from 'react-mixin'

//  import style from 'StemApp/client/css/StemApp.import.css'
@ReactMixin.decorate(Formsy.Mixin)  //  eslint-disable-line
export default class MySelect extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    validationError: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    if (window.DEBUG == true) {
      console.log('Should Component Update MySelect')
    }
    //  console.log('Should Component Update MySelect')
    if ((nextProps.name !== this.props.name) || (nextProps.validationError !== this.props.validationError) ||
      (nextState._value !== this.state._value)) {
      //  debugger
      return true
    }
    //  debugger
    return false
    //  return nextProps.id !== this.props.id
  };
  changeValue (event) {
    this.setValue(event.currentTarget.value)
  }
  render () {
    if (window.DEBUG == true) {
      console.log('MySelect Rendered')
    }

    const options = this.props.options.map((option, i) => (
      <option key={option.title + option.value} value={option.value}>
        {option.title}
      </option>
    ))
    //  let className = this.showRequired() ? 'required' : this.showError() ? 'error' : null
    //  Add this just in case there has been an external error from server side invalidation, all other cases error is passed by props
    let errorMessage = this.state._externalError ? this.state._externalError[0] : this.props.validationError
    return (
      <li>
        <div className='item-content'>
          <div className='item-inner'>
            <div className='item-title label'>{this.props.label}</div>
            <div className='item-input'>
              <select type={this.props.type} name={this.props.name} onChange={this.changeValue.bind(this)} value={this.getValue()} placeholder={this.props.label}>
                {options}
              </select>
            </div>
            <div className='item-after' style={{color: 'red'}}>{this.state._isValid || this.state._isPristine ? '' : errorMessage}</div>
          </div>
        </div>
      </li>
    )
  }
}
