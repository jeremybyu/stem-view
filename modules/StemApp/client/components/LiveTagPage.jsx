import { Component, PropTypes } from 'react'
import ReactMixin from 'react-mixin'
import LiveTagList from './LiveTag/LiveTagList'
//  import style from 'StemApp/client/css/StemApp.import.css'
@ReactMixin.decorate(TrackerReact)  //  eslint-disable-line
export default class LiveTagPage extends Component {
  static propTypes = {
    user: PropTypes.object,
    auth: React.PropTypes.bool
  };

  constructor (props, context) {
    super(props)
    this.state = {
      subscription: {
        liveTags: Meteor.subscribe('mmodbus_liveTags')
      },
      hideCompleted: false
    }
  }
  getTags () {
    return MmodbusUtils.collections.LiveTags.find({}, {fields: {modifiedAt: 0}}).fetch()
  }

  componentWillUnmount () {
    this.state.subscription.tags.stop()
  }
  render () {
    let content = null
    if (!Meteor.isServer && !this.state.subscription.liveTags.ready()) {
      // loading
      content = (
        <div className='content-block' style={{textAlign: 'center'}}>
          <span style={{width: '42px', height: '42px'}} className='preloader'/>
        </div>
      )
    } else {
      content = (
          <LiveTagList tags={this.getTags()}/>
      )
    }
    return (
    <div className='page-content'>
      <div className='content-block-title'>List of Live Tags, Read Only</div>
      {content}
    </div>
    )
  }
}
