import ReactMixin from 'react-mixin'
import { Component, PropTypes } from 'react'
import * as Utils from '../../utils/utils'
import MyArrayItem from './MyArrayItem'

//  import style from 'StemApp/client/css/StemApp.import.css'
@ReactMixin.decorate(TrackerReact)  //  eslint-disable-line
export default class MyArray extends Component {
  static propTypes = {
    schema: PropTypes.object.isRequired,
    removeClickHandler: PropTypes.func.isRequired,
    addClickHandler: PropTypes.func.isRequired,
    count: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    if (window.DEBUG === true) {
      console.log('Should Component Update MyArray')
    }
    if ((nextProps.count !== this.props.count)) {
      //  debugger
      return true
    }
    //  debugger
    return false
    //  return nextProps.id !== this.props.id
  };
  changeValue (event) {
    this.setValue(event.currentTarget.value)
  }
  render () {
    if (window.DEBUG === true) {
      console.log('Array render called')
    }
    //  let className = this.showRequired() ? 'required' : this.showError() ? 'error' : null
    //  Add this just in case there has been an external error from server side invalidation, all other cases error is passed by props
    //  console.log('Array render called')
    let array = []
    let deleteVisible = {float: 'right'}
    this.props.count === 1 ? deleteVisible.display = 'none' : deleteVisible.display = 'inline'

    for (let i = 0; i < this.props.count; i++) {
      let keyRow = Utils.genName(this.props.name, i)
      array.push(
        <div key={keyRow}>
          <li className='item-divider'>
              {`${this.props.label} ${i + 1}`}
              <a style={deleteVisible} onClick={this.props.removeClickHandler.bind(this, i)} className='button color-red'>Delete</a>
          </li>
          <MyArrayItem schema={this.props.schema} index={i}/>
        </div>
      )
    }
    return (
      <div className='list-block'>
        <ul>
          {array}
          <li className='item-divider'>
              Add More Parameters
              <a style={{float: 'right'}} onClick={this.props.addClickHandler.bind(this)} className='button color-green'>Add</a>
          </li>
        </ul>
      </div>
    )
  }
}
