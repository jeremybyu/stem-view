import { Component, PropTypes } from 'react'
import LiveTagItem from './LiveTagItem'

export default class LiveTagList extends Component {
  static propTypes = {
    tags: PropTypes.array.isRequired
  };

  render () {
    return (
      <div className='list-block'>
        <ul>
          {this.props.tags.map(tag => <LiveTagItem key={tag._id} tag={tag}/>)}
        </ul>
      </div>
    )
  }
}
