import { Component } from 'react'
import ReactMixin from 'react-mixin'
import MyForm from './Form/MyForm'
//  import style from 'StemApp/client/css/StemApp.import.css'
Session.setDefault('form_id', null)
//  window.DEBUG = true

@ReactMixin.decorate(TrackerReact)  //  eslint-disable-line
export default class FormPage extends Component {
  constructor (props, context) {
    super(props)
    this.state = {
      title: this.getTitle()
    }
  }
  getTag = () => {
    let tagName = MmodbusUtils.collections.Tags.findOne({_id: Session.get('form_id')})
    return tagName
  };
  getTitle = () => {
    let title
    if (Session.get('form_id') === null) {
      title = 'Create New Tag'
    } else {
      let tagName = this.getTag().tag
      title = `Edit ${tagName}`
    }
    return title
  };
  render () {
    let Form
    if (Session.get('form_id') !== null) {
      Form = (
        <MyForm update={true} id={Session.get('form_id')} tag={this.getTag()}/>
      )
    } else {
      Form = (
        <MyForm update={false} id={null} tag={null}/>
        )
    }

    return (
    <div className='page-content'>
      <div className='content-block-title'>
        {this.getTitle()}
      </div>
      {Form}
    </div>
    )
  }
}
