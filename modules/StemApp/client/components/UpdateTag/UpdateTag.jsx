import { Component, PropTypes } from 'react'
import ReadOnlyInput from '../General/ReadOnlyInput'
import ReactMixin from 'react-mixin'
import UpdateInput from '../General/UpdateInput'
import UpdateMessage from './UpdateMessage'

@ReactMixin.decorate(TrackerReact)
export default class UpdateTag extends Component {
  static propTypes = {
    tagParam: PropTypes.string.isRequired,
    auth: PropTypes.bool.isRequired,
    readOnly: PropTypes.bool
  };
  constructor (props, context) {
    super(props)
    this.state = {
      updatePending: false,
      responseObject: {error: '', success: false}
    }
  }
  getTag () {
    return MmodbusUtils.collections.LiveTags.findOne({tag_param: this.props.tagParam}, {fields: {modifiedAt: 0}})
  }
  update = () => {
    console.log(`Submit update for tag: ${this.props.tagParam} with value ${this.refs.tag.state.value}`)
    let updatePromise = Meteor.promise('UpdateTagParam', this.props.tagParam, this.refs.tag.state.value)
    this.setState({updatePending: true})
    updatePromise.then((responseObject) => {
      this.setState({updatePending: false})
      this.setState({responseObject: responseObject})
    })
  };
  render () {
    if (window.DEBUG === true) {
      console.log('Render UpdateTag called')
    }
    let fullTag = this.getTag()
    if (fullTag === undefined) {
      return (<li className='accordion-item'>
        <a href='#' className='item-content item-link'>
          <div className='item-inner'>
            <div className='item-title'>Invalid Tag: {this.props.tagParam}</div>
          </div>
        </a>
        <div className='accordion-item-content'>
          <div className='list-block'>
            Please Add the above tag and perform a server reset.
          </div>
        </div>
      </li>)
    } else {
      return (
        <li className='accordion-item'>
          <a href='#' className='item-content item-link'>
            <div className='item-inner'>
              <div className='item-title'>{fullTag.description}</div>
            </div>
            <ReadOnlyInput value={fullTag.value} dataType={fullTag.dataType}/>
          </a>
          <div className='accordion-item-content'>
            <div className='list-block'>
              <ul>
                <li>
                  <UpdateInput auth={this.auth} ref='tag' tag={fullTag}/>
                </li>
                <li>
                  <div className='item-content'>
                    <div className='item-inner'>
                      <a onClick={this.update} className={'button color-blue' + (this.props.auth ? '' : ' disabled')}>Update</a>
                      <UpdateMessage auth={this.props.auth} success={this.state.responseObject.success} message={this.state.responseObject.error} pending={this.state.updatePending}/>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </li>
      )
    }
  }
}
