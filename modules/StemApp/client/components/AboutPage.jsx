import { Component } from 'react'
//  import style from 'StemApp/client/css/StemApp.import.css'

export default class FormExample extends Component {
  render () {
    return (
    <div className='page-content'>
      <div className='content-block'>
        <h4>
          STEM Launcher- Science, Technology, Engineering, and Math
        </h4>
        <p>
          The STEM project is to help launch kids and young adults into the STEM disciplines.
        </p>
        <p>
          This mobile-first application interfaces between the user and the control center. With this applicaton you can view live data from all the sensors
          connnected to the control center. You can even control (i.e turn pumps on/off) once you are signed in.
        </p>
      </div>
    </div>
    )
  }
}
