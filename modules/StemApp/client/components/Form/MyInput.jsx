import Formsy from 'formsy-react'
import { Component, PropTypes } from 'react'
import ReactMixin from 'react-mixin'

//  import style from 'StemApp/client/css/StemApp.import.css'
@ReactMixin.decorate(Formsy.Mixin)  //  eslint-disable-line
export default class MyInput extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    validationError: PropTypes.string.isRequired
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    if (window.DEBUG === true) {
      console.log('Should Component Update MyInput')
    }
    //  console.log('Should Component Update MyInput')
    if ((nextProps.name !== this.props.name) || (nextProps.validationError !== this.props.validationError) ||
      (nextState._value !== this.state._value) || nextState._isValid !== this.state._isValid) {
      //  debugger
      return true
    }
    //  debugger
    return false
    //  return nextProps.id !== this.props.id
  };
  changeValue (event) {
    let value = event.currentTarget.value
    if (this.props.type === 'number') {
      value = Number(value)
    }
    this.setValue(isNaN(value) ? event.currentTarget.value : value)
  }
  render () {
    if (window.DEBUG === true) {
      console.log('MyInput render called')
    }
    //  console.log('MyInput render called')
    //  let className = this.showRequired() ? 'required' : this.showError() ? 'error' : null
    //  Add this just in case there has been an external error from server side invalidation, all other cases error is passed by props
    let errorMessage = this.state._externalError ? this.state._externalError[0] : this.props.validationError
    return (
      <li>
        <div className='item-content'>
          <div className='item-inner'>
            <div className='item-title label'>{this.props.label}</div>
            <div className='item-input'>
              <input type={this.props.type} name={this.props.name} placeholder={this.props.label} value={this.getValue()} onChange={this.changeValue.bind(this)}/>
            </div>
            <div className='item-after' style={{color: 'red'}}>{this.state._isValid || this.state._isPristine ? '' : errorMessage}</div>
          </div>
        </div>
      </li>
    )
  }
}
