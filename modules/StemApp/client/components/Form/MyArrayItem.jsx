import ReactMixin from 'react-mixin'
import { Component, PropTypes } from 'react'
import * as Utils from '../../utils/utils'
import MyInput from './MyInput'
import MySelect from './MySelect'

//  import style from 'StemApp/client/css/StemApp.import.css'
@ReactMixin.decorate(TrackerReact)  //  eslint-disable-line
export default class MyArrayItem extends Component {
  static propTypes = {
    schema: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired
  };
  constructor (props, context) {
    super(props)
    let tableAllowedValues = this.props.schema['params.$.table'].allowedValues
    let dataTypeAllowedValues = this.props.schema['params.$.dataType'].allowedValues
    let tableOptionsArray = tableAllowedValues.map((value) => {
      let obj = {}
      obj.value = value
      obj.title = value
      return obj
    })
    let dataTypeOptionsArray = dataTypeAllowedValues.map((value) => {
      let obj = {}
      obj.value = value
      obj.title = value
      return obj
    })
    this.state = {
      tableOptionsArray: tableOptionsArray,
      dataTypeOptionsArray: dataTypeOptionsArray,
      validationContext: MmodbusUtils.collections.Tags.simpleSchema().namedContext('tagForm')
    }
  }
  shouldComponentUpdate = (nextProps, nextState) => {
    if (window.DEBUG == true) {
      console.log('Should Component Update MyArrayItem')
    }

    if ((nextProps.index !== this.props.index)) {
      //  debugger
      return true
    }
    //  debugger
    return false
    //  return nextProps.id !== this.props.id
  };
  render () {
    if (window.DEBUG == true) {
      console.log('Render MyArrayItem')
    }
    //  let className = this.showRequired() ? 'required' : this.showError() ? 'error' : null
    //  Add this just in case there has been an external error from server side invalidation, all other cases error is passed by props
    //  console.log('Array Item render Called')
    //  store props for less lengthy referencing
    let index = this.props.index
    let validationContext = this.state.validationContext

    let keynName = Utils.genName('params', index, 'name')
    let keytName = Utils.genName('params', index, 'table')
    let keyaName = Utils.genName('params', index, 'address')
    let keydName = Utils.genName('params', index, 'dataType')

    let keynName2 = Utils.genArrayName('params', index, 'name')
    let keytName2 = Utils.genArrayName('params', index, 'table')
    let keyaName2 = Utils.genArrayName('params', index, 'address')
    let keydName2 = Utils.genArrayName('params', index, 'dataType')

    //  //  console.log(keynName)
    let nNameErrMsg = validationContext.keyErrorMessage(keynName)
    let tNameErrMsg = validationContext.keyErrorMessage(keytName)
    let aNameErrMsg = validationContext.keyErrorMessage(keyaName)
    let dNameErrMsg = validationContext.keyErrorMessage(keydName)
    return (
      <div>
        <MyInput name={keynName2} type='text' validations={'validateKey:' + keynName} label='Name' validationError={nNameErrMsg} required/>
        <MySelect value='Coil' options={this.state.tableOptionsArray} name={keytName2} type='text' validations={'validateKey:' + keytName} label='Table' validationError={tNameErrMsg}/>
        <MyInput name={keyaName2} type='number' validations={'validateKey:' + keyaName} label='Address' validationError={aNameErrMsg} required/>
        <MySelect value='Bit' options={this.state.dataTypeOptionsArray} name={keydName2} type='text' validations={'validateKey:' + keydName} label='Data Type' validationError={dNameErrMsg}/>
      </div>
    )
  }
}
