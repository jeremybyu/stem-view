import { Component, PropTypes } from 'react'

export default class UpdateMessage extends Component {
  static propTypes = {
    message: PropTypes.string,
    success: PropTypes.bool.isRequired,
    pending: PropTypes.bool.isRequired,
    auth: PropTypes.bool.isRequired
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    return true
  };
  getMessageStyle = () => {
    let divStyle = {color: 'red'}
    if (this.props.success) {
      divStyle.color = 'green'
    } else if (this.props.pending) {
      divStyle.color = 'initial'
    } else if (!this.props.auth) {
      divStyle.color = ''
    }
    return divStyle
  };
  render () {
    if (window.DEBUG === true) {
      console.log('UpdateMessage render called')
    }
    let content = this.props.message === null ? 'Success!' : this.props.message
    if (this.props.pending) {
      content = (<i style={{color: 'initial'}} className='fa fa-spinner fa-spin'></i>)
    }
    if (!this.props.auth) {
      content = 'Please sign in...'
    }
    return (
      <div style={this.getMessageStyle()}>{content}</div>
    )
  }
}
