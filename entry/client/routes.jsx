/* global React, ReactRouterSSR */
import { Route } from 'react-router'
import todoRoutes from 'StemApp/client/routes'

ReactRouterSSR.Run(
  <Route>
    {todoRoutes}
  </Route>
  , {
    rootElement: 'react-app',
    rootElementType: 'span'
  })
