import { Component, PropTypes } from 'react'
import ReadOnlyInput from '../General/ReadOnlyInput'

export default class LiveTagItem extends Component {
  static propTypes = {
    tag: PropTypes.object.isRequired
  };

  render () {
    return (
      <li className='swipeout' >
        <div className='swipeout-content item-content' style={{}}>
          <div className='item-inner'>
            <div className='item-title'>
              Tag: {this.props.tag.tag_param}
            </div>
            <ReadOnlyInput value={this.props.tag.value} dataType={this.props.tag.dataType}/>
          </div>
        </div>
      </li>
    )
  }
}
