import { modbusInstance } from './server/mmodbus/initialize'
let TagSchema = MmodbusUtils.collections.Tags.simpleSchema()
let validationContext = TagSchema.namedContext('tagForm')

Meteor.methods({
  TagTitleAvailable: function (tagName) {
    let tagsWithTitle = MmodbusUtils.collections.Tags.findOne({tag: tagName})
    // console.log(tagsWithTitle)
    if (tagsWithTitle) {
      return {unique: false, id: tagsWithTitle._id}
    } else {
      return {unique: true, id: null}
    }
  },
  InsertTag: (data) => {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized')
    }
    let cleanData = TagSchema.clean(data)
    let valid = validationContext.validate(cleanData)
    if (valid) {
      MmodbusUtils.collections.Tags.insert(cleanData)
    } else {
      return false
    }
  },
  UpdateTag: (id, data) => {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized')
    }
    let cleanData = TagSchema.clean(data)
    let valid = validationContext.validate(cleanData)
    if (valid) {
      MmodbusUtils.collections.Tags.update(id, {$set: cleanData})
    } else {
      return false
    }
  },
  DeleteTag: (id) => {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized')
    }
    MmodbusUtils.collections.Tags.remove({'_id': id})
  },
  UpdateTagParam: (tagParam, value) => {
    if (!Meteor.userId()) {
      throw new Meteor.Error('not-authorized')
    }
    let responseObject = modbusInstance.updateValue(tagParam, value)
    if (responseObject.success) {
      MmodbusUtils.collections.LiveTags.update({
        tag_param: tagParam
      }, {
        $set: {
          value: value,
          quality: true,
          modifiedAt: new Date()
        }
      })
    }
    return responseObject
  }
})
