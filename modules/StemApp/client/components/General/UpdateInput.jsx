import { Component, PropTypes } from 'react'

export default class UpdateInput extends Component {
  static propTypes = {
    tag: PropTypes.object.isRequired
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    return true
  };
  isBit = () => {
    return this.props.tag.dataType === 'Bit'
  };
  constructor (props, context) {
    super(props)
    this.state = {
      value: 0
    }
  }
  handleChange = (event) => {
    console.log(`Handle Change`)
    //  debugger
    let value = this.isBit() ? !event.target.parentElement.children[0].checked : event.target.value
    this.setState({value: Number(value)})
  };
  render () {
    if (window.DEBUG === true) {
      console.log('UpdateInput render called')
    }
    let content = (
        <input style={{float: 'right', paddingRight: '12px'}} step='any' type='number' onChange={this.handleChange} name={this.props.tag.tag_param} placeholder='Update Value'/>
    )

    if (this.isBit()) {
      content = (
        <label className='label-switch'>
          <input defaultChecked={this.props.tag.value === 1} name={this.props.tag.tag_param} type='checkbox'/>
          <div onClick={this.handleChange} className='checkbox'></div>
        </label>
      )
    }
    //  let className = this.showRequired() ? 'required' : this.showError() ? 'error' : null
    return (
      <div className='item-content'>
        <div style={{paddingRight: '1px'}} className='item-inner'>
          <div style={{width: 'auto'}} className='item-title label'><b>Update</b> {this.props.tag.tag_param}</div>
          <div style={{maxWidth: '135px', float: 'right', textAlign: 'right', paddingLeft: '10px'}} className='item-input'>
            {content}
          </div>
        </div>
      </div>

    )
  }
}
