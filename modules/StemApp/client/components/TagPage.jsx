import { Component, PropTypes } from 'react'
import ReactMixin from 'react-mixin'
import TagList from './Tag/TagList'
//  import style from 'StemApp/client/css/StemApp.import.css'
@ReactMixin.decorate(TrackerReact)  //  eslint-disable-line
export default class TagPage extends Component {
  static propTypes = {
    user: PropTypes.object,
    auth: React.PropTypes.bool
  };

  constructor (props, context) {
    super(props)
    this.state = {
      subscription: {
        tags: Meteor.subscribe('mmodbus_tags')
      },
      hideCompleted: false
    }
  }
  getTags () {
    return MmodbusUtils.collections.Tags.find({}).fetch()
  }

  componentWillUnmount () {
    this.state.subscription.tags.stop()
  }
  render () {
    let content = null
    if (!Meteor.isServer && !this.state.subscription.tags.ready()) {
      // loading
      content = (
        <div className='content-block' style={{textAlign: 'center'}}>
          <span style={{width: '42px', height: '42px'}} className='preloader'/>
        </div>
      )
    } else {
      content = (
          <TagList tags={this.getTags()} user={this.props.user} auth={this.props.auth}/>
      )
    }
    return (
    <div className='page-content'>
      <div className='content-block-title'>List of Tags, Swipe to Delete or Edit</div>
      {content}
    </div>
    )
  }
}
