import ReactMixin from 'react-mixin'
import Formsy from 'formsy-react'
import { Component, PropTypes } from 'react'

import * as Utils from '../../utils/utils.js'
import MyInput from './MyInput'
import MyArray from './MyArray'
// //  console.log(LocalCollection)

Formsy.addValidationRule('validateKey', function (values, value, otherField) {
  let object = {}
  object[otherField] = value
  // //  console.log(object)
  let valid = MmodbusUtils.collections.Tags.simpleSchema().namedContext('tagForm').validateOne(object, otherField)
  return valid
  //  return true
})

//  import style from 'StemApp/client/css/StemApp.import.css'
@ReactMixin.decorate(TrackerReact)  //  eslint-disable-line
export default class MyForm extends Component {
  static propTypes = {
    update: PropTypes.bool.isRequired,
    id: PropTypes.string,
    tag: PropTypes.object
  };
  constructor (props, context) {
    super(props)
    this.state = {
      canSubmit: false,
      validationContext: MmodbusUtils.collections.Tags.simpleSchema().namedContext('tagForm'),
      numParams: 0,
      schema: MmodbusUtils.collections.Tags.simpleSchema()
    }
  }
  shouldComponentUpdate = (nextProps, nextState) => {
    if (window.DEBUG === true) {
      console.log('Should Component Update MyForm')
    }
    if (nextProps.id !== this.props.id || nextProps.update !== this.props.update || nextState.canSubmit !== this.state.canSubmit ||
      nextState.numParams !== this.state.numParams) {
      //  debugger
      return true
    }
    // debugger
    return false
    //  return nextProps.id !== this.props.id
  };
  componentDidMount () {
    // TODO Get rid global variables and sesson variables
    //  This is the only way that I could do this.  I should have freaking used redux....
    //  Session wont work, Local Collections wont work, function throttling wont work.
    //  Sister React component communcation is an issue, I need to learn this....
    window.myForm = this
  }
  checkUpdate = () => {
    //  console.log(`Checking update with this context ${this}`)
    if (this.props.id) {
      let tag = MmodbusUtils.collections.Tags.findOne({_id: this.props.id})
      //  //  console.log(tag)
      //  console.log(this.refs.form)
      this.refs.form.reset(Utils.fromObj(tag))
    }
  };
  getParamsCount = () => {
    if (this.props.tag) {
      return this.props.tag.params.length + this.state.numParams
    }
    return this.state.numParams + 1
  };
  enableButton = () => {
    this.setState({
      canSubmit: true
    })
  };
  changeValue = (event) => {
    this.setValue(event.currentTarget.value)
  };
  disableButton = () => {
    this.setState({
      canSubmit: false
    })
  };
  submitHelper = () => {
    this.refs.form.submit()
    //  //  console.log(this.refs.form.getModel())
  };
  submit = (model, resetForm, invalidateForm) => {
    //  console.log('Inside Submit')
    //  console.log(model)
    let currID = this.props.id
    let isUpdate = this.props.update
    let cleanModel = this.state.schema.clean(model)
    //  console.log(cleanModel)
    let valid = this.state.validationContext.validate(cleanModel)

    if (valid) {  //  Form is valid, but still need to check if Book Title is unique
      Meteor.call('TagTitleAvailable', cleanModel.tag, (error, result) => {
        if (error) {
          //  console.log(error) //  Consider invalidating Form
        } else {
          if (result.unique) {
            if (isUpdate === true) {
              //  MmodbusUtils.collections.Tag.update(currID, {$set: cleanModel})
              Meteor.call('UpdateTag', currID, cleanModel)
            } else {
              Meteor.call('InsertTag', cleanModel)
            }
            resetForm()
            //  Consider Adding a success message or something
          } else {
            if (isUpdate === true || result.id === currID) {
              //  MmodbusUtils.collections.Tag.update(currID, {$set: cleanModel})  //  We are simply updating the same document
              Meteor.call('UpdateTag', currID, cleanModel)
            } else {
              // Guess this book is already taken!
              //  console.log('Tag is already taken')
              invalidateForm({tag: 'This title is already taken'})
            }
          }
        }
      })
    } else {
      //  Its not valid, not sure how i got here...
      //  console.log('What the hell do I do now?')
    }
  };
  addParam = () => {
    let numParams = this.state.numParams
    this.setState({numParams: numParams + 1})
  };
  removeParam (index) {
    //  console.log(`Should remove this index: ${index}`)
    // let numParams = this.state.numParams
    // this.setState({numParams: numParams - 1})
    // //  Basically if the deleted Param is not the last in the array, we need to do this tricky stuff below
    // if (index + 1 !== numParams) {
    //   //  Dummy model until we get Formsy.getModel working
    //   let model = this.refs.form.getModel()
    //   // let model = {title: 'j1', publisher: 'j2', Params: [{fname:'1',lname:'1'},{fname:'2',lname:'2'}]}
    //   //  let Params = model.Params.slice() //make a copy, something about best pracice immuttable data, yada yada
    //   model.Params.splice(index, 1) // remove index
    //   //  model.Params = Params //re assign
    //   let formKeys = Form2Obj.fromObj(model)
    //   this.refs.form.resetModel(formKeys)
    // }
  }
  render () {
    if (window.DEBUG === true) {
      console.log('Render MyForm called')
    }
    let baseButtonClass = 'button'
    let disabledButtonClass = 'disabled'
    let buttonClass = this.state.canSubmit ? baseButtonClass : Utils.appendClass(baseButtonClass, disabledButtonClass)

    let tagErrMsg = this.state.validationContext.keyErrorMessage('tag')
    let descErrMsg = this.state.validationContext.keyErrorMessage('description')
    //  let pubErrMsg = this.state.validationContext.keyErrorMessage('publisher')
    return (
        <Formsy.Form ref='form' id='my-form' className='content-block' onValid={this.enableButton} onValidSubmit={this.submit} onInvalid={this.disableButton}>
          <div className='list-block'>
            <ul>
              <MyInput validations='validateKey:tag' name='tag' type='text' label='Tag' validationError={tagErrMsg} required/>
              <MyInput validations='validateKey:description' name='description' type='text' label='Description' validationError={descErrMsg} required/>
            </ul>
        </div>
        <div className='content-block-title'>
          Parameters
        </div>
        <MyArray schema={this.state.schema.schema()} removeClickHandler={this.removeParam} addClickHandler={this.addParam} count={this.getParamsCount()} name='params' label='Parameters'/>

      <div className='content-block'>
          <a href='#' onClick={this.submitHelper} className={buttonClass}>Submit</a>
        </div>
      </Formsy.Form>
    )
  }
}
