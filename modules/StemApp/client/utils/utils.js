export function appendClass (baseClass, newClass) {
  return `${baseClass} ${newClass}`
}
export function genName (s0, s1, s2 = undefined) {
  if (s2 === undefined) {
    return `${s0}.${s1}`
  } else {
    return `${s0}.${s1}.${s2}`
  }
}
export function genArrayName (s0, s1, s2) {
  return `${s0}[${s1}][${s2}]`
}

export function throttle (callback, limit) {
  let wait = false // Initially, we're not waiting
  return function () { // We return a throttled function
    if (!wait) { // If we're not waiting
      callback.call() // Execute users function
      wait = true // Prevent future invocations
      setTimeout(function () { // After a period of time
        wait = false // And allow future invocations
      }, limit)
    }
  }
}
export function toObj (source) {
  return Object.keys(source).reduce(function (output, key) {
    var parentKey = key.match(/[^\[]*/i)
    var paths = key.match(/\[.*?\]/g) || []
    paths = [parentKey[0]].concat(paths).map(function (key) {
      return key.replace(/\[|\]/g, '')
    })
    var currentPath = output
    while (paths.length) {
      var pathKey = paths.shift()

      if (pathKey in currentPath) {
        currentPath = currentPath[pathKey]
      } else {
        currentPath[pathKey] = paths.length ? isNaN(paths[0]) ? {} : [] : source[key]
        currentPath = currentPath[pathKey]
      }
    }

    return output
  }, {})
}

export function fromObj (obj) {
  function recur (newObj, propName, currVal) {
    if (Array.isArray(currVal) || Object.prototype.toString.call(currVal) === '[object Object]') {
      Object.keys(currVal).forEach(function (v) {
        recur(newObj, propName + '[' + v + ']', currVal[v])
      })
      return newObj
    }

    newObj[propName] = currVal
    return newObj
  }

  var keys = Object.keys(obj)
  return keys.reduce(function (newObj, propName) {
    return recur(newObj, propName, obj[propName])
  }, {})
}
