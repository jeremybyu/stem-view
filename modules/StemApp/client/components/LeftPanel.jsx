import {Component} from 'react'
import style from 'StemApp/client/css/StemApp.import.css'

export default class LeftPanel extends Component {
  render () {
    return (
      <div className='panel panel-left layout-dark panel-reveal'>
        <div className='content-block-title'>STEM Launcher</div>
        <div className='content-block'>
          <p className>Science, Technology, Engineering, and Math.</p>
        </div>
        <div className='content-block-title'>Page Links</div>
        <div className='list-block'>
          <ul>
            <li>
              <a href='#tag' target='_blank' className='item-link ajax close-panel'>
                <div className='item-content'>
                  <div className='item-media'><i className={'fa fa-pencil-square-o fa-2x ' + style.editIcon}/></div>
                  <div className='item-inner'>
                    <div className='item-title'>View and Edit Tags</div>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href='#liveTag' target='_blank' className='item-link ajax close-panel'>
                <div className='item-content'>
                  <div className='item-media'><i className={'fa fa-list fa-2x ' + style.editIcon}/></div>
                  <div className='item-inner'>
                    <div className='item-title'>View Live Tags</div>
                  </div>
                </div>
              </a>
            </li>
            <li>
              <a href='#about' target='_blank' className='item-link ajax close-panel'>
                <div className='item-content'>
                  <div className='item-media'><i className={'fa fa-info fa-2x ' + style.editIcon}/></div>
                  <div className='item-inner'>
                    <div className='item-title'>About Us</div>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}
