import { Component, PropTypes } from 'react'
import ReactMixin from 'react-mixin'
import Helmet from 'react-helmet'
import F7 from './f7/js/f7'

import LoginScreen from './components/LoginScreen'
import LeftPanel from './components/LeftPanel'
import RightPanel from './components/RightPanel'
import FormPage from './components/FormPage'
import AboutPage from './components/AboutPage'
import TagPage from './components/TagPage'
import LiveTagPage from './components/LiveTagPage'

import * as Utils from './utils/utils'

// Thanks to TrackerReact all our reactive meteor calls render also reactively in react (i.e. user())
@ReactMixin.decorate(TrackerReact)
export default class StemApp extends Component {
  static propTypes = {
    children: PropTypes.any.isRequired
  };

  constructor (props, context) {
    super(props)
    this.state = {
      f7: null,
      shownDisconnect: false
    }
  }

  auth () {
    // Use auth() for auth checks.
    // Fast-Render transports userId() on SSR directly to the client
    // (if he is authed). So we use this for auth checks - faster than Meteor.user()
    return Meteor.userId() ? true : false // eslint-disable-line
  }

  user () {
    // Only use user() to get the user object.
    // Meteor.userId is also available on the server via a cookie thanks to fast-render
    let userId = Meteor.userId()

    if (userId) {
      // But to SSR user info (i.e. username), we can not get the user object via Meteor.user() on the Server
      // (a reactive data source), so we query the user object via the collection handler (not reactive).
      if (Meteor.isServer) {
        return Meteor.users._collection.findOne({_id: userId})
      }
      return Meteor.user()
    } else {
      return null
    }
  }

  loggingIn () {
    if (!Meteor.isServer) {
      return Meteor.loggingIn()
    } else {
      return false
    }
  }

  handleLoginDialog () {
    if (this.user()) {
      Meteor.logout()
    } else {
      this.state.f7.loginScreen()
    }
  }
  componentDidMount = () => {
    if (!Meteor.isServer) {
      let app = new F7()
      //  this.setState({app: app})

      // Add main View
      let view = app.addView('.view-main', { // eslint-disable-line
        // Enable dynamic Navbar
        dynamicNavbar: true,
        // Enable Dom Cache so we can use all inline pages
        domCache: true,
        swipePanel: 'left'
      })

      // Pass instance to state to pass to children.
      // We are anyhow able to always get the instance via new F7()
      this.setState({f7: app})
      //  TODO Need to find a way to get rid of global state, or unmount react component with F7
      // Because we are using static page, the F7 router does not unmount the react components page
      // This causes a serious issue where state is never reset.  We must do it manually for ourselves.
      // The only way that I could do this is expose a global variable to the MyForm component.
      // If I used redux this might work easier.
      app.onPageAfterBack('form', function (page) {
        //  console.log('Leaving Form Page')
        Session.set('form_id', null)
        window.myForm.setState({numParams: 0})
        window.myForm.refs.form.reset({})
      })
      app.onPageAfterAnimation('form', function (page) {
        //  console.log('Form Page: onPageAfterAnimation')
        if (window.myForm.props.update) {
          let tagID = Session.get('form_id')
          let tag = MmodbusUtils.collections.Tags.findOne({_id: tagID})
          //  console.log(tag)
          window.myForm.refs.form.reset(Utils.fromObj(tag))
        }
      })
      // app.onPageInit('form', function (page) {
      //   console.log('Entering Form Page, Init')
      // })
      // app.onPageReinit('form', function (page) {
      //   console.log('Entering Form Page, ReInit')
      // })
      let notificationHandle = Tracker.autorun(() => {
        if (Meteor.status().connected === false && this.state.shownDisconnect === false) {
          //  console.log('Should show notification')
          this.setState({shownDisconnect: true})
          this.state.f7.addNotification({
            title: 'Server Disconnect',
            message: 'You have been disconnected from the server. Please Check WiFi connection.'
          })
        } else if (Meteor.status().connected === true) {
          //  console.log('Should reset Notification')
          this.setState({shownDisconnect: false})
        }
      })
    }
  };

  render () {
    if (window.DEBUG === true) {
      console.log('Index (StemApp) render called')
    }
    let connectionStatus = Meteor.status().connected
    let createNewTagButton = (<div></div>)
    // conditionally show the Create new Tag button if logged in.
    if (this.auth()) {
      createNewTagButton = (
        <a href='#form' onClick={this.createNewTag} className='button active'>
          Create Tag
        </a>
      )
    }
    let notConnected = <div></div>
    if (connectionStatus === false) {
      notConnected = <div style={{color: 'red'}}>Connecting...<i className='fa fa-spinner fa-spin'></i></div>
    }
    return (
      <span className='f7-main'>
        <Helmet title='STEM Launcher' meta={[{
          name: 'description',
          content: 'Jumpstarting kids education in Science, Engineering, and Math'
        }
        ]}/>
        {/* Status bar overlay for fullscreen mode*/}
        <div className='statusbar-overlay'/>
        {/* Panels overlay*/}
        <div className='panel-overlay'/>
        {/* Left panel with reveal effect*/}
        <LeftPanel/>
        {/* Right panel with cover effect*/}
        <RightPanel/>
        {/* Views*/}
        <div className='views'>
          {/* Your main view, should have 'view-main' class*/}
          <div className='view view-main'>
            {/* Top Navbar*/}
            <div className='navbar'>
              {/* Navbar inner for Index page*/}
              <div data-page='index' className='navbar-inner'>
                {/* We have home navbar without left link*/}
                <div className='left sliding'>
                  <a href='#' data-panel='left' className='open-panel icon-only'>
                    <i className='icon icon-bars'></i>
                  </a>
                </div>
                <div className='center sliding'>{connectionStatus ? 'Overview' : notConnected}</div>
                <div className='right'>
                  {/* Right link contains only icon - additional 'icon-only' class*/}
                  <a href='#' onClick={this.handleLoginDialog.bind(this)} className={'button ' + (this.auth()
                    ? 'active'
                    : '')}>
                    {this.auth()
                      ? 'Sign Out'
                      : 'Sign In'}
                  </a>
                  {/* <a href='#' className='link icon-only open-panel'><i className='icon icon-bars'/></a>*/}
                </div>
              </div>
              {/* Navbar inner for About page*/}
              <div data-page='about' className='navbar-inner cached'>
                <div className='left sliding'>
                  <a href='#' className='back link'>
                    <i className='icon icon-back'/>
                    <span>Back</span>
                  </a>
                </div>
                <div className='center sliding'>{connectionStatus ? 'About Us' : notConnected}</div>
              </div>
              {/* Navbar inner for Tag page*/}
              <div data-page='tag' className='navbar-inner cached'>
                <div className='left sliding'>
                  <a href='#' className='back link'>
                    <i className='icon icon-back'/>
                    <span>Back</span>
                  </a>
                </div>
                <div className='center sliding'>{connectionStatus ? 'View & Edit Tags' : notConnected}</div>
                <div className='right'>
                  {createNewTagButton}
                </div>
              </div>
              {/* Navbar inner for Live Tag page*/}
              <div data-page='liveTag' className='navbar-inner cached'>
                <div className='left sliding'>
                  <a href='#' className='back link'>
                    <i className='icon icon-back'/>
                    <span>Back</span>
                  </a>
                </div>
                <div className='center sliding'>{connectionStatus ? 'View Live Tags' : notConnected}</div>
              </div>
              {/* Navbar inner for Form page*/}
              <div data-page='form' className='navbar-inner cached'>
                <div className='left sliding'>
                  <a href='#' className='back link'>
                    <i className='icon icon-back'/>
                    <span>Back</span>
                  </a>
                </div>
                <div className='center sliding'>{connectionStatus ? 'Create or Edit Tags' : notConnected}</div>
              </div>
            </div>
            {/* Pages, because we need fixed-through navbar and toolbar, it has additional appropriate classes*/}
            <div className='pages navbar-through toolbar-through'>
              {/* Index Page*/}
              <div data-page='index' className='page'>
                {/* Scrollable page content*/}
                {React.cloneElement(this.props.children, {
                  user: this.user(),
                  auth: this.auth()
                })}
              </div>
              {/* About Page*/}
              <div data-page='about' className='page cached'>
                <AboutPage/>
              </div>
              {/* Form Page*/}
              <div data-page='form' className='page cached'>
                <FormPage/>
              </div>
              {/* Tag Page*/}
              <div data-page='tag' className='page cached'>
                <TagPage user={this.user()} auth={this.auth()}/>
              </div>
              {/* Live Tag Page*/}
              <div data-page='liveTag' className='page cached'>
                <LiveTagPage user={this.user()} auth={this.auth()}/>
              </div>
            </div>

            {/* Bottom Toolbar*/}
            <div className='toolbar'>
              <div className='toolbar-inner'>
                <a href='#' data-panel='left' className='link open-panel'>Info</a>
                <a href='#' data-panel='right' className='link open-panel'>Options</a>
              </div>
            </div>
          </div>
        </div>
        <LoginScreen user={this.user()} loggingIn={this.loggingIn()} f7={this.state.f7}/>
      </span>
    )
  }
}
