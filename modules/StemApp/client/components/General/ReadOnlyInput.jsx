import { Component, PropTypes } from 'react'

export default class ReadOnlyInput extends Component {
  static propTypes = {
    value: PropTypes.number.isRequired,
    dataType: PropTypes.string.isRequired
  };
  shouldComponentUpdate = (nextProps, nextState) => {
    return true
  };
  isBit = () => {
    return this.props.dataType === 'Bit'
  };
  render () {
    if (window.DEBUG === true) {
      console.log('ReadOnlyInput render called')
    }
    let content = (<div className=''>
      {this.props.value}
    </div>)

    if (this.isBit()) {
      let toggle = (<input readOnly type='checkbox'/>)
      if (this.props.value === 1) {
        toggle = (<input sytle={{pointerEvents: 'none'}} readOnly checked type='checkbox'/>)
      }
      content = (
        <label className='label-switch disabled'>
          {toggle}
          <div className='checkbox'></div>
        </label>
      )
    }
    //  let className = this.showRequired() ? 'required' : this.showError() ? 'error' : null
    return (
      <div className='item-after'>
        <div style={{minWidth: '55px', textAlign: 'center'}} className='item-input'>
        {content}
        </div>
      </div>
    )
  }
}
