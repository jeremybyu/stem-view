### STEM-VIEW

This is a user interface to interact with the STEM Project (Science, Technology, Engineering, and Math). The STEM project is an initiative to help
children and young adults learn more about science and use it in practical ways.

Currently this app is set up to allow individuals use mobile devices and computers to connect to the STEM control system.  With this app they can receive instant updates of the STEM system (temperature, flow rates) as well as issue commands (close valves, turn on pumps). This app makes heavy use of the author-created package [jeremybyu:mmodbus](https://github.com/JeremyBYU/modbus.git)
which allows Modbus communication to the control system.

### Getting Started
Clone this project to start a mobile web project using Meteor, React.js, Framework7 and Webpack. [Install Meteor]( https://www.meteor.com/install), than

1. `git clone https://jeremybyu@bitbucket.org/jeremybyu/stem-view.git`
1. `cd stem-view`
1. `meteor`

One package is missing however ```jeremybyu:mmodbus```.  This package is not published yet and will have to be retrieved through [here](https://github.com/JeremyBYU/modbus.git)

### Features
* Nice Clean Mobile interface using Framework 7, IOS Design
* Fully data reactive design, allowing students to see instant updates of the process
* User Access Control.  Certain actions are not permitted unless signed in
* Instant Form validation when individuals create "Tags".
* Check out the Documentation folder to see Videos and Pictures of the app in action!

### Development Notes
* Uses Webpack and Meteor to instantly compile assets
* Uses okgrow:promise package for ES6 promises while awaiting server responses to commands.
* Hot Reload is available
* Uses a MongoDB database to store data
