import { Route, IndexRoute } from 'react-router'

import StemApp from './index'
import StemMain from './StemMain'
// import LeftPanel from './components/LeftPanel'
// import RightPanel from './components/RightPanel'

export default (
<Route>
  <Route path='/' component={StemApp}>
    <IndexRoute component={StemMain} />
  </Route>
</Route>
)
