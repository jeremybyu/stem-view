import { Component, PropTypes } from 'react'
import TagItem from './TagItem'

export default class TagList extends Component {
  static propTypes = {
    tags: PropTypes.array.isRequired,
    user: PropTypes.object,
    auth: PropTypes.bool.isRequired
  };

  render () {
    return (
      <div className='list-block'>
        <ul>
          {this.props.tags.map(tag => <TagItem key={tag._id} tag={tag} user={this.props.user} auth={this.props.auth} />)}
        </ul>
        <div className='list-block-label'>
          Swipe left on a Tag to delete, edit.
        </div>
      </div>
    )
  }
}
