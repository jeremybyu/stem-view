import {Component, PropTypes} from 'react'
import ReactMixin from 'react-mixin'
import UpdateTag from './components/UpdateTag/UpdateTag'
@ReactMixin.decorate(TrackerReact) //  eslint-disable-line
export default class StemMain extends Component {
  static propTypes = {
    user: PropTypes.object,
    auth: PropTypes.bool
  };
  constructor (props, context) {
    super(props)
    this.state = {
      subscription: {
        liveTags: Meteor.subscribe('mmodbus_liveTags')
      }
    }
  }
  componentWillUnmount () {
    //  console.log('Unmount TodoMain')
    this.state.subscription.liveTags.stop()
  }
  render () {
    let content = null
    if (!Meteor.isServer && !this.state.subscription.liveTags.ready()) {
      // loading
      content = (
        <div className='content-block' style={{
          textAlign: 'center'
        }}>
          <span style={{
            width: '42px',
            height: '42px'
          }} className='preloader'/>
        </div>
      )
    } else {
      content = (
        <div className='content-block' style={{
          textAlign: 'center'
        }}>
          <div className='content-block-title'>View and Control Process</div>
          <div className='list-block accordion-list'>
            <ul>
              <UpdateTag tagParam='XV01_PV' auth={this.props.auth} readOnly={false}/>
              <UpdateTag tagParam='TI01_PV' auth={this.props.auth} readOnly={false}/>
              <UpdateTag tagParam='TI01_SP' auth={this.props.auth} readOnly={false}/>
            </ul>
          </div>
        </div>
      )
    }

    return (
      <div className='page-content'>
        {content}
      </div>
    )
  }
};
